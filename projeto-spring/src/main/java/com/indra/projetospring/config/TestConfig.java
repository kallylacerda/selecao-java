//package com.indra.projetospring.config;
//
//import java.util.Arrays;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;
//
//import com.indra.projetospring.entities.CashFlow;
//import com.indra.projetospring.entities.County;
//import com.indra.projetospring.entities.Dealer;
//import com.indra.projetospring.entities.Establishment;
//import com.indra.projetospring.entities.Flag;
//import com.indra.projetospring.entities.Region;
//import com.indra.projetospring.entities.State;
//import com.indra.projetospring.entities.User;
//import com.indra.projetospring.entities.enums.Fuel;
//import com.indra.projetospring.entities.enums.MeasureUnit;
//import com.indra.projetospring.repositories.CashFlowRepository;
//import com.indra.projetospring.repositories.CountyRepository;
//import com.indra.projetospring.repositories.DealerRepository;
//import com.indra.projetospring.repositories.EstablishmentRepository;
//import com.indra.projetospring.repositories.FlagRepository;
//import com.indra.projetospring.repositories.RegionRepository;
//import com.indra.projetospring.repositories.StateRepository;
//import com.indra.projetospring.repositories.UserRepository;
//import com.indra.projetospring.utils.FormatDate;
//
//@Configuration
//@Profile("test")
//public class TestConfig implements CommandLineRunner {
//
//	@Autowired
//	private UserRepository userRepository;
//	
//	@Autowired
//	private CountyRepository countyRepository;
//	
//	@Autowired
//	private EstablishmentRepository establishmentRepository;
//	
//	@Autowired
//	private FlagRepository flagRepository;
//	
//	@Autowired
//	private DealerRepository dealerRepository;
//	
//	@Autowired
//	private CashFlowRepository cashFlowRepository;
//	
//	@Autowired
//	private RegionRepository regionRepository;
//	
//	@Autowired
//	private StateRepository stateRepository;
//
//	@Override
//	public void run(String... args) throws Exception {
//		
//		User u1 = new User(null, "Maria Brown", "maria@gmail.com", "123456");
//		User u2 = new User(null, "Alex Green", "alex@gmail.com", "123456");
//		
//		userRepository.saveAll(Arrays.asList(u1, u2));
//		
//		Region r1 = new Region(null, "NE", "Nordeste");
//		Region r2 = new Region(null, "NO", "Noroeste");
//		Region r3 = new Region(null, "S", "Sul");
//		
//		regionRepository.saveAll(Arrays.asList(r1, r2, r3));
//		
//		State s1 = new State(null, "PB", "Paraíba", r1);
//		State s2 = new State(null, "RN", "Rio Grande do Norte", r1);
//		State s3 = new State(null, "AC", "Acre", r2);
//		State s4 = new State(null, "RS", "Rio Grande do Sul", r3);
//		State s5 = new State(null, "SC", "Santa Catarina", r3);
//		
//		stateRepository.saveAll(Arrays.asList(s1, s2, s3, s4, s5));
//		
//		County c1 = new County(null, "João Pessoa", s1);
//		County c2 = new County(null, "Cabedelo", s1);
//		County c3 = new County(null, "Santa Rita", s1);
//		County c4 = new County(null, "Natal", s2);
//		County c5 = new County(null, "Pipa", s2);
//		County c6 = new County(null, "Caxias do Sul", s4);
//		County c7 = new County(null, "Rio Branco", s3);
//		County c8 = new County(null, "Mata Redonda", s4);
//		County c9 = new County(null, "Serra Gaucha", s5);
//		County c10 = new County(null, "Labarra", s5);
//		
//		countyRepository.saveAll(Arrays.asList(c1, c2, c3, c4, c5, c6, c7, c8, c9, c10));
//		
//		Establishment e1 = new Establishment(null, "Abastecedora LTDA");
//		Establishment e2 = new Establishment(null, "Dolor sit amet LTDA");
//		Establishment e3 = new Establishment(null, "Platea dictums LTDA");
//		Establishment e4 = new Establishment(null, "Sed libero LTDA");
//		Establishment e5 = new Establishment(null, "Ipsum enim LTDA");
//		Establishment e6 = new Establishment(null, "Libero nibh LTDA");
//		Establishment e7 = new Establishment(null, "Nulla id LTDA");
//		Establishment e8 = new Establishment(null, "Proin eget");
//		Establishment e9 = new Establishment(null, "Integer dictum LTDA");
//		Establishment e10 = new Establishment(null, "Nullam feugiat LTDA");
//		Establishment e11 = new Establishment(null, "Sapien in neque LTDA");
//		
//		establishmentRepository.saveAll(Arrays.asList(e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11));
//		
//		Flag f1 = new Flag(null, "Petrobras");
//		Flag f2 = new Flag(null, "Ipiranga");
//		Flag f3 = new Flag(null, "Branca");
//		Flag f4 = new Flag(null, "Raizen");
//		
//		flagRepository.saveAll(Arrays.asList(f1, f2, f3, f4));
//		
//		Dealer d2 = new Dealer(4123L, c1, e2, f1);
//		Dealer d1 = new Dealer(12423L, c1, e1, f1);
//		Dealer d3 = new Dealer(12313L, c1, e3, f1);
//		Dealer d4 = new Dealer(214L, c7, e4, f2);
//		Dealer d5 = new Dealer(1829L, c1, e5, f1);
//		Dealer d6 = new Dealer(1237L, c9, e6, f4);
//		Dealer d7 = new Dealer(7887L, c10, e7, f1);
//		Dealer d8 = new Dealer(1234L, c1, e8, f3);
//		Dealer d9 = new Dealer(1679L, c1, e9, f1);
//		Dealer d10 = new Dealer(2419L, c1, e10, f1);
//		Dealer d11 = new Dealer(92130L, c1, e11, f1);
//		Dealer d12 = new Dealer(81923L, c1, e1, f2);
//		Dealer d13 = new Dealer(9904L, c3, e1, f2);
//		Dealer d14 = new Dealer(77283L, c1, e4, f2);
//		Dealer d15 = new Dealer(66948L, c4, e6, f3);
//		Dealer d16 = new Dealer(3370L, c1, e7, f2);
//		Dealer d17 = new Dealer(9881L, c5, e1, f4);
//		Dealer d18 = new Dealer(7755L, c1, e8, f1);
//		
//		dealerRepository.saveAll(Arrays.asList(d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18));
//		
//		CashFlow h1 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.valueOf("DIESEL"), 3.66, 3.71, MeasureUnit.valueOf("R$ / litro"), d1);
//		CashFlow h2 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d5);
//		CashFlow h3 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d2);
//		CashFlow h4 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d10);
//		CashFlow h5 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d11);
//		CashFlow h6 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d16);
//		CashFlow h7 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d10);
//		CashFlow h8 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d18);
//		CashFlow h9 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d10);
//		CashFlow h10 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d2);
//		CashFlow h11 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d3);
//		CashFlow h12 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d6);
//		CashFlow h13 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d9);
//		CashFlow h14 = new CashFlow(null, FormatDate.formatDate("20/06/2018"), Fuel.DIESEL, 3.66, 3.71, MeasureUnit.RS_L, d10);
//		
//		cashFlowRepository.saveAll(Arrays.asList(h1, h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h13, h14));
//	}
//}
