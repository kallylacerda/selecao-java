package com.indra.projetospring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.indra.projetospring.services.CsvImportService;

@RestController
@RequestMapping("/upload")
public class CsvImportController {

	@Autowired
	private CsvImportService csvImportService;
	
	@PostMapping
	public void upload (@RequestParam("file") MultipartFile file, @RequestParam("delimiter") String delimiter) {
		csvImportService.insertCsv(file, delimiter);
	}
	
}
