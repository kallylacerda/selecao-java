package com.indra.projetospring.controllers;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indra.projetospring.dtos.HistoryDTO;
import com.indra.projetospring.services.HistoryInfoService;

@RestController
@RequestMapping("/histories/infos")
public class HistoryInfoController {

	@Autowired
	private HistoryInfoService service;
	
	@GetMapping("/counties/{countyName}/avg-price")
	public ResponseEntity<Double> getAvgPricePerCountyName(@PathVariable String countyName){
		Double resp = service.getAvgPricePerCountyName(countyName.toUpperCase());
		return ResponseEntity.ok().body(resp);
	}
	
	@GetMapping("/counties/avg-price")
	public ResponseEntity<Map<String, Map<String,Double>>> getAvgPricePerCounty(){
		Map<String, Map<String,Double>> resp = service.getAvgPricePerCounty();
		return ResponseEntity.ok().body(resp);
	}
	
	@GetMapping("/regions/{regionAcronym}")
	public ResponseEntity<List<HistoryDTO>> getInfoByRegionAcronym(@PathVariable String regionAcronym){
		List<HistoryDTO> resp = service.getInfoByRegionAcronym(regionAcronym.toUpperCase());
		return ResponseEntity.ok().body(resp);
	}
	
	@GetMapping("/regions")
	public ResponseEntity<Map<String, List<HistoryDTO>>> getInfoByRegion(){
		Map<String, List<HistoryDTO>> resp = service.getInfoByRegion();
		return ResponseEntity.ok().body(resp);
	}
	
	@GetMapping("/establishments/{establishmentCorporateName}")
	public ResponseEntity<List<HistoryDTO>> getInfoByEstablishmentName(@PathVariable String establishmentCorporateName){
		List<HistoryDTO> resp = service.getInfoByEstablishmentName(establishmentCorporateName.toUpperCase());
		return ResponseEntity.ok().body(resp);
	}
	
	@GetMapping("/establishments")
	public ResponseEntity<Map<String, List<HistoryDTO>>> getInfoByEstablishment(){
		Map<String, List<HistoryDTO>> resp = service.getInfoByEstablishment();
		return ResponseEntity.ok().body(resp);
	}
	
	@GetMapping("/collection-dates")
	public ResponseEntity<Map<LocalDate, List<HistoryDTO>>> getInfoByCollectionDate(){
		Map<LocalDate, List<HistoryDTO>> resp = service.getInfoByCollectionDate();
		return ResponseEntity.ok().body(resp);
	}
	
	@GetMapping("/flags/avg-price")
	public ResponseEntity<Map<String, Map<String,Double>>> getAvgPricePerFlag(){
		Map<String, Map<String,Double>> resp = service.getAvgPricePerFlag();
		return ResponseEntity.ok().body(resp);
	}
	
}
