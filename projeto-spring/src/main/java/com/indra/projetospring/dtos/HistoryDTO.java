package com.indra.projetospring.dtos;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.indra.projetospring.entities.CashFlow;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class HistoryDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	@NotNull @NotEmpty
	private String regionAcronym;
	@NotNull @NotEmpty
	private String stateAcronym;	
	@NotNull @NotEmpty
	private String countyName;
	@NotNull @NotEmpty
	private String establishmentCorporateName;
	@NotNull @NotEmpty
	private Long dealerCode;	
	@NotNull @NotEmpty
	private String fuelName;
	@NotNull @NotEmpty
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate collectionDate;
	private Double cashFlowBuyingPrice;
	private Double cashFlowSellingPrice;
	private String measureUnitName;
	@NotNull @NotEmpty
	private String flagName;
	
	public static List<HistoryDTO> convert(List<CashFlow> cashFlows){
		List<HistoryDTO> historiesDTO = new ArrayList<>();
		for(CashFlow cashFlow : cashFlows) {
			historiesDTO.add(new HistoryDTO(cashFlow.getId(),
					cashFlow.getDealer().getCounty().getState().getRegion().getAcronym(),
					cashFlow.getDealer().getCounty().getState().getAcronym(),
					cashFlow.getDealer().getCounty().getName(),
					cashFlow.getDealer().getEstablishment().getCorporateName(),
					cashFlow.getDealer().getCode(), 
					cashFlow.getFuel().getName(),
					cashFlow.getCollectionDate(),
					cashFlow.getBuyingPrice(),
					cashFlow.getSellingPrice(),
					cashFlow.getMeasureUnit().getName(),
					cashFlow.getDealer().getFlag().getName()));
		}
		return historiesDTO;
	}

	public static HistoryDTO convert(CashFlow cashFlow) {
		return new HistoryDTO(cashFlow.getId(),
						cashFlow.getDealer().getCounty().getState().getRegion().getAcronym(),
						cashFlow.getDealer().getCounty().getState().getAcronym(),
						cashFlow.getDealer().getCounty().getName(),
						cashFlow.getDealer().getEstablishment().getCorporateName(),
						cashFlow.getDealer().getCode(), 
						cashFlow.getFuel().getName(),
						cashFlow.getCollectionDate(),
						cashFlow.getBuyingPrice(),
						cashFlow.getSellingPrice(),
						cashFlow.getMeasureUnit().getName(),
						cashFlow.getDealer().getFlag().getName());
	}
	
	public void toUpperCase() {
		setRegionAcronym(getRegionAcronym().toUpperCase());
		setStateAcronym(getStateAcronym().toUpperCase());
		setCountyName(getCountyName().toUpperCase());
		setEstablishmentCorporateName(getEstablishmentCorporateName().toUpperCase());
		setFuelName(getFuelName().toUpperCase());
		setMeasureUnitName(getMeasureUnitName().toUpperCase());
		setFlagName(getFlagName().toUpperCase());
	}
	
}
