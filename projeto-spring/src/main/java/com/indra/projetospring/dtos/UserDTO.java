package com.indra.projetospring.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.indra.projetospring.entities.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class UserDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private String username;
	
	public static List<UserDTO> convert(List<User> users){
		List<UserDTO> usersDTO = new ArrayList<>();
		for(User user : users) {
			usersDTO.add(new UserDTO(user.getId(), user.getName(), user.getUsername()));
		}
		return usersDTO;
	}

	public static UserDTO convert(User user) {
		return new UserDTO(user.getId(), user.getName(), user.getUsername());
	}
}
