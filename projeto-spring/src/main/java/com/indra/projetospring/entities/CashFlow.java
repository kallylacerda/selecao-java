package com.indra.projetospring.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.indra.projetospring.entities.enums.Fuel;
import com.indra.projetospring.entities.enums.MeasureUnit;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="cash_flow")
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CashFlow implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include 
	@Getter	private Long id;
	@EqualsAndHashCode.Include 
	@Getter @Setter private LocalDate collectionDate;
	@Getter @Setter private Double buyingPrice;
	@Getter @Setter private Double sellingPrice;
	
	@ManyToOne
	@JoinColumn(name="code_dealer")
	@Getter @Setter private Dealer dealer;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	@Getter @Setter private User user;
	
	private Integer fuel;
	private Integer measureUnit;

	public CashFlow(Long id, LocalDate collectionDate, Fuel fuel, Double buyingPrice, Double sellingPrice,
			MeasureUnit measureUnit, Dealer dealer) {
		super();
		this.id = id;
		this.collectionDate = collectionDate;
		setFuel(fuel);
		this.buyingPrice = buyingPrice;
		this.sellingPrice = sellingPrice;
		setMeasureUnit(measureUnit);
		this.dealer = dealer;
	}

	public Fuel getFuel() {
		return Fuel.valueOf(fuel);
	}

	public void setFuel(Fuel fuel) {
		if(fuel != null) {
			this.fuel = fuel.getCode();
		}
	}

	public MeasureUnit getMeasureUnit() {
		return MeasureUnit.valueOf(measureUnit);
	}

	public void setMeasureUnit(MeasureUnit measureUnit) {
		if(measureUnit != null) {
			this.measureUnit = measureUnit.getCode();
		}
	}
	
}
