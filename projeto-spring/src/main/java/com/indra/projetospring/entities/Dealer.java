package com.indra.projetospring.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="dealer")
@NoArgsConstructor @AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Dealer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@EqualsAndHashCode.Include	
//	@Column(name = "code_dealer", unique = true)
	@Getter @Setter	private Long code;
	
	@ManyToOne
	@Getter @Setter private County county;
	
	@ManyToOne
	@Getter @Setter private Establishment establishment;
	
	@ManyToOne
	@Getter @Setter private Flag flag;
}
