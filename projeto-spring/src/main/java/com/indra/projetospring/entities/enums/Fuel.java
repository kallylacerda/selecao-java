package com.indra.projetospring.entities.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Fuel {
	DIESEL(1, "DIESEL"),
	DIESEL_S10(2, "DIESEL S10"),
	ETANOL(3, "ETANOL"),
	GASOLINA(4, "GASOLINA"),
	GNV(5, "GNV");
	
	private int code;
	private String name;
	
	public static Fuel valueOf(int code) {
		for(Fuel value : Fuel.values()) {
			if(value.getCode() == code) {
				return value;
			}
		}
		throw new IllegalArgumentException("Invalid Fuel code");
	}
	
	public static Fuel stringToEnum(String name) {
		for(Fuel value : Fuel.values()) {
			if(value.getName().equals(name)) {
				return value;
			}
		}
		throw new IllegalArgumentException("Invalid Fuel");
	}
}
