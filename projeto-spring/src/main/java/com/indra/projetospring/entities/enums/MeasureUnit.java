package com.indra.projetospring.entities.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MeasureUnit {
	RS_L(1, "R$ / litro");

	private int code;
	private String name;
	
	public static MeasureUnit valueOf(int code) {
		for(MeasureUnit value : MeasureUnit.values()) {
			if(value.getCode() == code) {
				return value;
			}
		}
		throw new IllegalArgumentException("Invalid Fuel code");
	}

	public static MeasureUnit stringToEnum(String name) {
		for(MeasureUnit value : MeasureUnit.values()) {
			if(value.getName().equals(name)) {
				return value;
			}
		}
		throw new IllegalArgumentException("Invalid Fuel");
	}
}
