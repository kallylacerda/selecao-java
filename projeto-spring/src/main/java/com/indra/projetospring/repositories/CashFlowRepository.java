package com.indra.projetospring.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.projetospring.entities.CashFlow;

@Repository
public interface CashFlowRepository extends JpaRepository<CashFlow, Long> {
	List<CashFlow> getByDealerCode(Long dealerCode);
	List<CashFlow> getByCollectionDate(LocalDate collectionDate);
}
