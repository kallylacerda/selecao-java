package com.indra.projetospring.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.projetospring.entities.County;

@Repository
public interface CountyRepository extends JpaRepository<County, Integer> {
	County getByName(String name);
	List<County> getByStateCode(Integer stateCode);
}
