package com.indra.projetospring.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.projetospring.entities.Dealer;

@Repository
public interface DealerRepository extends JpaRepository<Dealer, Long> {
	Dealer getByCode(Long code);
	List<Dealer> getByCountyCode(Integer countyCode);
	List<Dealer> getByEstablishmentId(Long establishmentId);
	List<Dealer> getByFlagId(Integer flagId);
}
