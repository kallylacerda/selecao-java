package com.indra.projetospring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.projetospring.entities.Flag;

@Repository
public interface FlagRepository extends JpaRepository<Flag, Integer> {
	Flag getByName(String name);
}
