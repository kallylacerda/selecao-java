package com.indra.projetospring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.projetospring.entities.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer> {
	Region getByAcronym(String acronym);
}
