package com.indra.projetospring.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.projetospring.entities.State;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {
	State getByAcronym(String acronym);
	List<State> getByRegionCode(Integer regionCode);
}
