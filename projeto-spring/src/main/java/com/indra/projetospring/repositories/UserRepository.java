package com.indra.projetospring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indra.projetospring.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
