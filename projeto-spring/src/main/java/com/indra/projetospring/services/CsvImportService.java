package com.indra.projetospring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.indra.projetospring.dtos.HistoryDTO;
import com.indra.projetospring.repositories.CashFlowRepository;
import com.indra.projetospring.repositories.CountyRepository;
import com.indra.projetospring.repositories.DealerRepository;
import com.indra.projetospring.repositories.EstablishmentRepository;
import com.indra.projetospring.repositories.FlagRepository;
import com.indra.projetospring.repositories.RegionRepository;
import com.indra.projetospring.repositories.StateRepository;
import com.indra.projetospring.utils.CsvImportUtil;
import com.indra.projetospring.utils.FormatDate;
import com.indra.projetospring.utils.InstantiateHistory;
import com.indra.projetospring.utils.RemoveNull;

@Service
public class CsvImportService {

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private StateRepository stateRepository;

	@Autowired
	private CountyRepository countyRepository;

	@Autowired
	private EstablishmentRepository establishmentRepository;

	@Autowired
	private DealerRepository dealerRepository;

	@Autowired
	private FlagRepository flagRepository;

	@Autowired
	private CashFlowRepository cashFlowRepository;

	public void insertCsv(MultipartFile file, String delimiter) {

		List<String> listCsv = CsvImportUtil.readCsv(file);
//		List<CashFlow> listCashFlow = new ArrayList<>();

		for (String line : listCsv) {
			try {
				line = line.replaceAll(",", ".");
				String[] record = line.split(delimiter);
				for (int i = 0; i < record.length; i++) {
					record[i] = record[i].trim();
				}
				
				HistoryDTO objDTO = new HistoryDTO(null, record[0], record[1], record[2], record[3], Long.parseLong(record[4]), record[5], FormatDate.formatDate(record[6]), RemoveNull.removeNullInStringAndPassToDouble(record[7]), RemoveNull.removeNullInStringAndPassToDouble(record[8]), record[9], record[10]);

				InstantiateHistory.instatiateHistory(objDTO, regionRepository, stateRepository, 
						countyRepository, establishmentRepository, dealerRepository, flagRepository, 
						cashFlowRepository);
			

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

//		cashFlowRepository.saveAll(listCashFlow);
	}

}
