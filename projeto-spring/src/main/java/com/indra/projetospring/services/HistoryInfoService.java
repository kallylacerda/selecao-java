package com.indra.projetospring.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indra.projetospring.dtos.HistoryDTO;
import com.indra.projetospring.entities.CashFlow;
import com.indra.projetospring.entities.County;
import com.indra.projetospring.entities.Dealer;
import com.indra.projetospring.entities.Establishment;
import com.indra.projetospring.entities.Flag;
import com.indra.projetospring.entities.Region;
import com.indra.projetospring.entities.State;
import com.indra.projetospring.repositories.CashFlowRepository;
import com.indra.projetospring.repositories.CountyRepository;
import com.indra.projetospring.repositories.DealerRepository;
import com.indra.projetospring.repositories.EstablishmentRepository;
import com.indra.projetospring.repositories.FlagRepository;
import com.indra.projetospring.repositories.RegionRepository;
import com.indra.projetospring.repositories.StateRepository;
import com.indra.projetospring.services.exceptions.ResourceNotFoundException;

@Service
public class HistoryInfoService {

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private StateRepository stateRepository;

	@Autowired
	private CountyRepository countyRepository;

	@Autowired
	private EstablishmentRepository establishmentRepository;

	@Autowired
	private DealerRepository dealerRepository;

	@Autowired
	private FlagRepository flagRepository;

	@Autowired
	private CashFlowRepository cashFlowRepository;

	public Double getAvgPricePerCountyName(String countyName) {
		try {
			County county = countyRepository.getByName(countyName);
			List<Dealer> dealers = dealerRepository.getByCountyCode(county.getCode());
			List<CashFlow> cashFlows = new ArrayList<>();
			Double priceSum = 0.0;

			for (Dealer dealer : dealers) {
				cashFlows.addAll(cashFlowRepository.getByDealerCode(dealer.getCode()));
			}

			for (CashFlow cf : cashFlows) {
				priceSum += cf.getSellingPrice();
			}

			return priceSum / cashFlows.size();
		} catch (NullPointerException e) {
			throw new ResourceNotFoundException(countyName);
		}
	}

	public Map<String, Map<String,Double>> getAvgPricePerCounty() {
		try {
			Map<String, Double> countyBuyingPrice = new HashMap<>();
			Map<String, Double> countySellingPrice = new HashMap<>();

			Map<String, Integer> countBuyingPriceByCounty = new HashMap<>();
			Map<String, Integer> countSellingPriceByCounty = new HashMap<>();

			List<County> counties = countyRepository.findAll();

			for (County county : counties) {
				List<Dealer> dealers = dealerRepository.getByCountyCode(county.getCode());
				for (Dealer dealer : dealers) {
					List<CashFlow> cashFlows = cashFlowRepository.getByDealerCode(dealer.getCode());
					for (CashFlow cashFlow : cashFlows) {
						if (countyBuyingPrice.keySet().contains(county.getName())) {
							if (cashFlow.getBuyingPrice() > 0.0) {
								countyBuyingPrice.put(county.getName(),
										countyBuyingPrice.get(county.getName()) + cashFlow.getBuyingPrice());
								countBuyingPriceByCounty.put(county.getName(),
										countBuyingPriceByCounty.get(county.getName()) + 1);
							}
							if (cashFlow.getSellingPrice() > 0.0) {
								countySellingPrice.put(county.getName(),
										countySellingPrice.get(county.getName()) + cashFlow.getSellingPrice());
								countSellingPriceByCounty.put(county.getName(),
										countSellingPriceByCounty.get(county.getName()) + 1);
							}
						} else {
							if (cashFlow.getBuyingPrice() > 0.0) {
								countyBuyingPrice.put(county.getName(), cashFlow.getBuyingPrice());
								countBuyingPriceByCounty.put(county.getName(), 1);
							}
							if (cashFlow.getSellingPrice() > 0.0) {
								countySellingPrice.put(county.getName(), cashFlow.getSellingPrice());
								countSellingPriceByCounty.put(county.getName(), 1);
							}
						}
					}
				}
				countyBuyingPrice.put(county.getName(),
						countyBuyingPrice.get(county.getName()) / countBuyingPriceByCounty.get(county.getName()));
				countySellingPrice.put(county.getName(),
						countySellingPrice.get(county.getName()) / countSellingPriceByCounty.get(county.getName()));
			}

			Map<String, Map<String,Double>> avgPrices = new HashMap<>();
			
			avgPrices.put("buyingPrice", countyBuyingPrice);
			avgPrices.put("sellingPrice", countySellingPrice);
			return avgPrices;
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<HistoryDTO> getInfoByRegionAcronym(String regionAcronym) {
		try {
			Region region = regionRepository.getByAcronym(regionAcronym);
			List<State> states = stateRepository.getByRegionCode(region.getCode());
			List<County> counties = new ArrayList<>();

			for (State state : states) {
				counties.addAll(countyRepository.getByStateCode(state.getCode()));
			}
			List<Dealer> dealers = new ArrayList<>();
			for (County county : counties) {
				dealers.addAll(dealerRepository.getByCountyCode(county.getCode()));
			}
			List<HistoryDTO> historyDTOs = new ArrayList<>();
			for (Dealer dealer : dealers) {
				historyDTOs.addAll(HistoryDTO.convert(cashFlowRepository.getByDealerCode(dealer.getCode())));
			}

			return historyDTOs;
		} catch (NullPointerException e) {
			throw new ResourceNotFoundException(regionAcronym);
		}
	}

	public Map<String, List<HistoryDTO>> getInfoByRegion() {
		try {
			Map<String, List<HistoryDTO>> historyDTOsByAcronym = new HashMap<>();

			List<Region> regions = regionRepository.findAll();
			for (Region region : regions) {
				List<State> states = stateRepository.getByRegionCode(region.getCode());
				List<County> counties = new ArrayList<>();

				for (State state : states) {
					counties.addAll(countyRepository.getByStateCode(state.getCode()));
				}
				List<Dealer> dealers = new ArrayList<>();
				for (County county : counties) {
					dealers.addAll(dealerRepository.getByCountyCode(county.getCode()));
				}
				for (Dealer dealer : dealers) {
					historyDTOsByAcronym.put(region.getAcronym(),
							HistoryDTO.convert(cashFlowRepository.getByDealerCode(dealer.getCode())));
				}

			}

			return historyDTOsByAcronym;
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<HistoryDTO> getInfoByEstablishmentName(String establishmentCorporateName) {
		try {
			Establishment establishment = establishmentRepository.getByCorporateName(establishmentCorporateName);
			List<Dealer> dealers = dealerRepository.getByEstablishmentId(establishment.getId());
			List<HistoryDTO> historyDTOs = new ArrayList<>();

			for (Dealer dealer : dealers) {
				historyDTOs.addAll(HistoryDTO.convert(cashFlowRepository.getByDealerCode(dealer.getCode())));
			}

			return historyDTOs;
		} catch (NullPointerException e) {
			throw new ResourceNotFoundException(establishmentCorporateName);
		}
	}

	public Map<String, List<HistoryDTO>> getInfoByEstablishment() {
		try {
			Map<String, List<HistoryDTO>> historyDTOsByEstablishment = new HashMap<>();
			
			List<Establishment> establishments = establishmentRepository.findAll();
			for (Establishment establishment : establishments) {
				List<Dealer> dealers = dealerRepository.getByEstablishmentId(establishment.getId());

				for (Dealer dealer : dealers) {
					historyDTOsByEstablishment.put(establishment.getCorporateName(), HistoryDTO.convert(cashFlowRepository.getByDealerCode(dealer.getCode())));
				}
			}

			return historyDTOsByEstablishment;
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Map<LocalDate, List<HistoryDTO>> getInfoByCollectionDate() {
		try {
			Map<LocalDate, List<HistoryDTO>> historyDTOsByCollectionDate = new HashMap<>();
			
			List<CashFlow> cashFlows = cashFlowRepository.findAll();
			
			for (CashFlow cashFlow : cashFlows) {
				historyDTOsByCollectionDate.put(cashFlow.getCollectionDate(), HistoryDTO.convert(cashFlowRepository.getByCollectionDate(cashFlow.getCollectionDate())));
				
			}

			return historyDTOsByCollectionDate;
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Map<String, Map<String,Double>> getAvgPricePerFlag() {
		try {
			Map<String, Double> flagBuyingPrice = new HashMap<>();
			Map<String, Double> flagSellingPrice = new HashMap<>();

			Map<String, Integer> countBuyingPriceByFlag = new HashMap<>();
			Map<String, Integer> countSellingPriceByFlag = new HashMap<>();

			List<Flag> flags = flagRepository.findAll();

			for (Flag flag : flags) {
				List<Dealer> dealers = dealerRepository.getByCountyCode(flag.getId());
				for (Dealer dealer : dealers) {
					List<CashFlow> cashFlows = cashFlowRepository.getByDealerCode(dealer.getCode());
					for (CashFlow cashFlow : cashFlows) {
						if (flagBuyingPrice.keySet().contains(flag.getName())) {
							if (cashFlow.getBuyingPrice() > 0.0) {
								flagBuyingPrice.put(flag.getName(),
										flagBuyingPrice.get(flag.getName()) + cashFlow.getBuyingPrice());
								countBuyingPriceByFlag.put(flag.getName(),
										countBuyingPriceByFlag.get(flag.getName()) + 1);
							}
							if (cashFlow.getSellingPrice() > 0.0) {
								flagSellingPrice.put(flag.getName(),
										flagSellingPrice.get(flag.getName()) + cashFlow.getSellingPrice());
								countSellingPriceByFlag.put(flag.getName(),
										countSellingPriceByFlag.get(flag.getName()) + 1);
							}
						} else {
							if (cashFlow.getBuyingPrice() > 0.0) {
								flagBuyingPrice.put(flag.getName(), cashFlow.getBuyingPrice());
								countBuyingPriceByFlag.put(flag.getName(), 1);
							}
							if (cashFlow.getSellingPrice() > 0.0) {
								flagSellingPrice.put(flag.getName(), cashFlow.getSellingPrice());
								countSellingPriceByFlag.put(flag.getName(), 1);
							}
						}
					}
				}
				flagBuyingPrice.put(flag.getName(),
						flagBuyingPrice.get(flag.getName()) / countBuyingPriceByFlag.get(flag.getName()));
				flagSellingPrice.put(flag.getName(),
						flagSellingPrice.get(flag.getName()) / countSellingPriceByFlag.get(flag.getName()));
			}

			Map<String, Map<String,Double>> avgPrices = new HashMap<>();
			
			avgPrices.put("buyingPrice", flagBuyingPrice);
			avgPrices.put("sellingPrice", flagSellingPrice);
			
			return avgPrices;
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return null;
	}
}
