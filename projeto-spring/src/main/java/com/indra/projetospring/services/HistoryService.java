package com.indra.projetospring.services;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.indra.projetospring.dtos.HistoryDTO;
import com.indra.projetospring.entities.CashFlow;
import com.indra.projetospring.entities.enums.Fuel;
import com.indra.projetospring.entities.enums.MeasureUnit;
import com.indra.projetospring.repositories.CashFlowRepository;
import com.indra.projetospring.repositories.CountyRepository;
import com.indra.projetospring.repositories.DealerRepository;
import com.indra.projetospring.repositories.EstablishmentRepository;
import com.indra.projetospring.repositories.FlagRepository;
import com.indra.projetospring.repositories.RegionRepository;
import com.indra.projetospring.repositories.StateRepository;
import com.indra.projetospring.services.exceptions.ResourceNotFoundException;
import com.indra.projetospring.services.exceptions.SameObjectFoundException;
import com.indra.projetospring.utils.InstantiateHistory;
import com.indra.projetospring.utils.RemoveNull;
import com.indra.projetospring.utils.SearchEqual;

@Service
public class HistoryService {

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private StateRepository stateRepository;

	@Autowired
	private CountyRepository countyRepository;

	@Autowired
	private EstablishmentRepository establishmentRepository;

	@Autowired
	private DealerRepository dealerRepository;

	@Autowired
	private FlagRepository flagRepository;

	@Autowired
	private CashFlowRepository cashFlowRepository;

	public List<HistoryDTO> findAll() {
		return HistoryDTO.convert(cashFlowRepository.findAll());
	}

	public HistoryDTO findById(Long id) {
		Optional<CashFlow> obj = cashFlowRepository.findById(id);

		return HistoryDTO.convert(obj.orElseThrow(() -> new ResourceNotFoundException(id)));
	}

	public HistoryDTO insert(HistoryDTO obj) {
		RemoveNull.removeNullInDouble(obj);
		Long id = SearchEqual.searchEqual(obj, findAll());
		
		if(id == null) {
			return HistoryDTO.convert(InstantiateHistory.instatiateHistory(obj, regionRepository, stateRepository, 
				countyRepository, establishmentRepository, dealerRepository, flagRepository, cashFlowRepository));
		}
		else
			throw new SameObjectFoundException(id);
	}

	public HistoryDTO update(Long id, HistoryDTO obj) {
		RemoveNull.removeNullInDouble(obj);
		try {
			CashFlow entity = cashFlowRepository.getOne(id);
			updateData(entity, obj);
			return HistoryDTO.convert(cashFlowRepository.save(entity));
		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException(id);
		}
	}

	private void updateData(CashFlow entity, HistoryDTO obj) {
		entity.setCollectionDate(obj.getCollectionDate());
		entity.setBuyingPrice(obj.getCashFlowBuyingPrice());
		entity.setSellingPrice(obj.getCashFlowSellingPrice());
		entity.setFuel(Fuel.stringToEnum(obj.getFuelName()));
		entity.setMeasureUnit(MeasureUnit.stringToEnum(obj.getMeasureUnitName()));
		entity.setDealer(dealerRepository.findById(obj.getDealerCode()).get());
	}

	public void delete(Long id) {
		try {
			cashFlowRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException(id);
		}
	}
}
