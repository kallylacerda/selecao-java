package com.indra.projetospring.services;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.indra.projetospring.dtos.UserDTO;
import com.indra.projetospring.entities.User;
import com.indra.projetospring.repositories.UserRepository;
import com.indra.projetospring.services.exceptions.ResourceNotFoundException;
import com.indra.projetospring.services.exceptions.SameObjectFoundException;
import com.indra.projetospring.utils.SearchEqual;

@Service
public class UserService {

	@Autowired
	private UserRepository repository;

	public List<UserDTO> findAll() {
		return UserDTO.convert(repository.findAll());
	}

	public UserDTO findById(Long id) {
		Optional<User> obj = repository.findById(id);

		return UserDTO.convert(obj.orElseThrow(() -> new ResourceNotFoundException(id)));
	}

	public UserDTO insert(User obj) {
		Long id = SearchEqual.searchEqual(obj, findAll());
		
		if(id == null) {
			return UserDTO.convert(repository.save(obj));
		}
		else
			throw new SameObjectFoundException(id);
	}

	public UserDTO update(Long id, User obj) {
		try {
			User entity = repository.getOne(id);
			updateData(entity, obj);
			return UserDTO.convert(repository.save(entity));
		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException(id);
		}
	}

	private void updateData(User entity, User obj) {
		entity.setName(obj.getName());
		entity.setUsername(obj.getUsername());
		entity.setPassword(obj.getPassword());
	}

	public void delete(Long id) {
		try {
			repository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException(id);
		}
	}
}
