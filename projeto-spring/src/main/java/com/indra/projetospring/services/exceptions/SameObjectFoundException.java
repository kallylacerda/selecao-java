package com.indra.projetospring.services.exceptions;


public class SameObjectFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SameObjectFoundException(Object id) {
		super("The same object has founded. Id " + id);
	}
}
