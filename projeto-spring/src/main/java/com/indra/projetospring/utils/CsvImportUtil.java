package com.indra.projetospring.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public abstract class CsvImportUtil {
	
	public static List<String> readCsv(MultipartFile file) {
		
		List<String> result = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream(), "cp1252"))) {
			// pula a primeira linha, dos cabeçalhos
			br.readLine();
			
			String line;
			while ((line = br.readLine()) != null) {
				result.add(line);
			}

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
		return result;
	}

}
