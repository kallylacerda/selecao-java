package com.indra.projetospring.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class FormatDate {
	
	private final static DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public static LocalDate formatDate(String stringDate) {
		return LocalDate.parse(stringDate, format);
	}
}
