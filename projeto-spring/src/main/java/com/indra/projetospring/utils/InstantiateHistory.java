package com.indra.projetospring.utils;

import com.indra.projetospring.dtos.HistoryDTO;
import com.indra.projetospring.entities.CashFlow;
import com.indra.projetospring.entities.County;
import com.indra.projetospring.entities.Dealer;
import com.indra.projetospring.entities.Establishment;
import com.indra.projetospring.entities.Flag;
import com.indra.projetospring.entities.Region;
import com.indra.projetospring.entities.State;
import com.indra.projetospring.entities.enums.Fuel;
import com.indra.projetospring.entities.enums.MeasureUnit;
import com.indra.projetospring.repositories.CashFlowRepository;
import com.indra.projetospring.repositories.CountyRepository;
import com.indra.projetospring.repositories.DealerRepository;
import com.indra.projetospring.repositories.EstablishmentRepository;
import com.indra.projetospring.repositories.FlagRepository;
import com.indra.projetospring.repositories.RegionRepository;
import com.indra.projetospring.repositories.StateRepository;

public abstract class InstantiateHistory {
	
	public static CashFlow instatiateHistory(HistoryDTO historyDTO, RegionRepository regionRepository, StateRepository stateRepository, CountyRepository countyRepository, 
			EstablishmentRepository establishmentRepository, DealerRepository dealerRepository, FlagRepository flagRepository,
			CashFlowRepository cashFlowRepository) {
		
		Region region = regionRepository.getByAcronym(historyDTO.getRegionAcronym());
		State state = stateRepository.getByAcronym(historyDTO.getStateAcronym());
		County county = countyRepository.getByName(historyDTO.getCountyName());
		Establishment establishment = establishmentRepository.getByCorporateName(historyDTO.getEstablishmentCorporateName());
		Dealer dealer = dealerRepository.getByCode(historyDTO.getDealerCode());
		Flag flag = flagRepository.getByName(historyDTO.getFlagName());

		if (region == null) {
			region = new Region();
			region.setAcronym(historyDTO.getRegionAcronym());
			region = regionRepository.save(region);
		}

		if (state == null) {
			state = new State();
			state.setAcronym(historyDTO.getStateAcronym());
			state.setRegion(region);
			state = stateRepository.save(state);
		}

		if (county == null) {
			county = new County();
			county.setName(historyDTO.getCountyName());
			county.setState(state);
			county = countyRepository.save(county);
		}

		if (establishment == null) {
			establishment = new Establishment();
			establishment.setCorporateName(historyDTO.getEstablishmentCorporateName());
			establishment = establishmentRepository.save(establishment);
		}

		if (flag == null) {
			flag = new Flag();
			flag.setName(historyDTO.getFlagName());
			flag = flagRepository.save(flag);

		}

		if (dealer == null) {
			dealer = new Dealer();
			dealer.setCode(historyDTO.getDealerCode());
			dealer.setCounty(county);
			dealer.setEstablishment(establishment);
			dealer.setFlag(flag);
			dealer = dealerRepository.save(dealer);
		}

		CashFlow cashFlow = new CashFlow();
		cashFlow.setCollectionDate(historyDTO.getCollectionDate());
		cashFlow.setFuel(Fuel.stringToEnum(historyDTO.getFuelName()));
		cashFlow.setBuyingPrice(historyDTO.getCashFlowBuyingPrice());
		cashFlow.setSellingPrice(historyDTO.getCashFlowSellingPrice());
		cashFlow.setMeasureUnit(MeasureUnit.stringToEnum(historyDTO.getMeasureUnitName()));
		cashFlow.setDealer(dealer);
		return cashFlowRepository.save(cashFlow);
		
	}
	
}
