package com.indra.projetospring.utils;

import com.indra.projetospring.dtos.HistoryDTO;

public abstract class RemoveNull {
	public static void removeNullInDouble (HistoryDTO obj) {
		obj.setCashFlowBuyingPrice(obj.getCashFlowBuyingPrice() != null ? obj.getCashFlowBuyingPrice() : 0.0);
		obj.setCashFlowSellingPrice(obj.getCashFlowSellingPrice() != null ? obj.getCashFlowSellingPrice() : 0.0);
	}
	
	public static Double removeNullInStringAndPassToDouble (String value) {
		return !value.isEmpty() ? Double.valueOf(value) : 0.0;
	}
}
