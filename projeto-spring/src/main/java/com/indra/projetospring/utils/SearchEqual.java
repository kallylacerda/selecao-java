package com.indra.projetospring.utils;

import java.util.List;

import com.indra.projetospring.dtos.HistoryDTO;
import com.indra.projetospring.dtos.UserDTO;
import com.indra.projetospring.entities.User;

public abstract class SearchEqual {
	
	public static Long searchEqual(HistoryDTO obj, List<HistoryDTO> list) {
		
		for(HistoryDTO item : list)
			if(obj.getCountyName().equals(item.getCountyName())
					&& obj.getEstablishmentCorporateName().equals(item.getEstablishmentCorporateName())
					&& obj.getDealerCode().equals(item.getDealerCode())
					&& obj.getFuelName().equals(item.getFuelName())
					&& obj.getCollectionDate().equals(item.getCollectionDate())
					&& obj.getCashFlowBuyingPrice().equals(item.getCashFlowBuyingPrice())
					&& obj.getCashFlowSellingPrice().equals(item.getCashFlowSellingPrice())
					&& obj.getFlagName().equals(item.getFlagName()))
				return item.getId();
		
		return null;
	}
	
	public static Long searchEqual(User obj, List<UserDTO> list) {
		
		for(UserDTO item : list) {
			if(obj.getUsername().equals(item.getUsername()))
				return item.getId();
		}
		
		return null;
	}
}
